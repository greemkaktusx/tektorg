<?php
/**
 * Задание 3.
 * Написать функцию, которая принимает в качестве аргументов число (количество строк в итоговом массиве) и символ
 * (для заполнения массива, см. пример), возвращает массив строк.
 * Пример:
 * build(6, "*");
 * Вернет массив:
 * [
 * ' * ',
 * ' *** ',
 * ' ***** ',
 * ' ******* ',
 * ' ********* ',
 * '***********',
 * ]
 */

/**
 * Построение дерева с заданным количеством строк и символов
 * @param int $rowsCount Количество строк
 * @param string $symbol Символ
 * @return array
 */
function build(int $rowsCount, string $symbol): array
{
    /** @var array $tree Итоговое дерево */
    $tree = [];

    for ($i = 1; $i <= $rowsCount; $i++) {
        $emptySymbolsCount = (2 * ($rowsCount - $i)) / 2;

        $tree[] =
            str_repeat(' ', $emptySymbolsCount) .
            str_repeat($symbol, (2 * $i - 1)) .
            str_repeat(' ', $emptySymbolsCount);
    }

    return $tree;
}

var_dump(build(6, '*'));