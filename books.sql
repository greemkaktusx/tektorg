/**
* Задание 2.
* Написать SQL запрос. Есть таблицы authors и books. Нужно вывести из них данные
* по авторам, количеству книг и наличию положительного рейтинга у книг. Формат
* вывода: имя автора, общее количество книг данного автора, признак наличия книг с
* положительным рейтингом (если rating более 4.0, то выводить 1, иначе - 0). Результаты
* нужно отсортировать по количеству в обратном порядке. Дополнительно вывести
* только те, у которых количество книг с положительным рейтингом более 1. Схемы:
*/

CREATE TABLE `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255),
	PRIMARY KEY (`id`)
);

CREATE TABLE `books` (
	`id` int NOT NULL AUTO_INCREMENT,
	`title` varchar(255),
	`year` int,
	`author_id` int,
	`rating` decimal(3, 1),
	PRIMARY KEY (`id`),
	FOREIGN KEY (author_id)
	REFERENCES authors(id)
);


# Выборка данных по авторам количеству книг и наличию положительного рейтинга.
SELECT 
	`authors`.`name`,
	COUNT(`books`.`id`) as `countBooks`,
	IF(COUNT(IF(`books`.`rating` > 4.0, 1, NULL)), 1, 0) as `goodRating`
FROM 
	`authors`
	JOIN `books` ON `books`.`author_id` = `authors`.`id`
GROUP BY 
	`books`.`author_id`
ORDER BY
	`countBooks` DESC

# Выборка данных по авторам, у которых количество книг с положительным рейтингом более 1.
SELECT 
	`authors`.`name`,
	COUNT(IF(`books`.`rating` > 4.0, 1, NULL)) as `countBooks`
FROM 
	`authors`
	JOIN `books` ON `books`.`author_id` = `authors`.`id`
GROUP BY 
	`books`.`author_id`
HAVING
	`countBooks` > 1
ORDER BY
	`countBooks` DESC