<?php
/**
 * Задание 1.
 * Написать функцию, которая на входе принимает строку из скобок, и возвращает true если все открытые скобки закрыты,
 * иначе - false. Возможные варианты скобок: ()[]{}
 * Пример:
 * "(){}[]" => true
 * "([{}])" => true
 * "(}" => false
 * "[(])" => false
 * "[({})](]" => false
 */

/**
 * Валидация скобок в строке
 * @param string $bracketsString Исходная строка со скобками
 * @return bool
 */
function checkBrackets(string $bracketsString): bool
{
    /** @var array $brackets Скобки для проверки */
    $brackets = [
        '(' => ')',
        '{' => '}',
        '[' => ']'
    ];

    /** @var array $openBrackets Открытые скобки */
    $openBrackets = [];

    /** @var int $i Индекс символа в строке */
    for ($i = 0; $i <= strlen($bracketsString) - 1; $i++) {
        /** @var string $symbol Символ строки */
        $symbol = substr($bracketsString, $i, 1);

        // Если это открытая скобка, то помещаем ее в очередь для проверки.
        // Если это закрытая скобка, то проверяем очередь и соответствие последней открытой скобке.
        // Иначе перебираем строку дальше
        if (isset($brackets[$symbol])) {
            array_push($openBrackets, $symbol);
        } elseif (in_array($symbol, array_values($brackets))) {
            if (!$openBrackets) {
                return false;
            }

            if ($brackets[array_pop($openBrackets)] !== $symbol) {
                return false;
            }
        }
    }

    if ($openBrackets) {
        return false;
    }

    return true;
}

var_dump(checkBrackets('(){}[]')); // true
var_dump(checkBrackets('([{}])')); // true
var_dump(checkBrackets('(}')); // false
var_dump(checkBrackets('[(])')); // false
var_dump(checkBrackets('[({})](]')); // false
var_dump(checkBrackets('(a+b)*25+[1]{2}')); // true